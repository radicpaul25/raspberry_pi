package domain.service;

import domain.entity.InfoEntity;

public interface InfoServiceInterface {
    public InfoEntity getInfo();
}
