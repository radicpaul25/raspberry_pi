package domain.service;

import domain.entity.InfoEntity;
import oshi.SystemInfo;
import util.dataclass.Disk;
import util.dataclass.Network;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class InfoService implements InfoServiceInterface {
    private InfoEntity infoEntity;
    public InfoService()
    {
        var sysInfo = new SystemInfo();
        var hardware = sysInfo.getHardware();
        var os = sysInfo.getOperatingSystem();
        os.getInternetProtocolStats().getConnections().get(0);
        var disks = hardware.getDiskStores();
        List<Disk> listDisk = new ArrayList<>();
        for (var disk : disks)
        {
            listDisk.add(
                    new Disk()
                        .withName(disk.getName())
                        .withSize(disk.getSize()));
        }
        List<Network> networks = new ArrayList<>();
        for (var net : hardware.getNetworkIFs())
        {
            if (net.getIPv4addr().length != 0)
                networks.add(new Network().withName(net.getName()).withIp(net.getIPv4addr()[0]));
        }
        infoEntity = new InfoEntity().withNumberOfCores(hardware.getProcessor().getLogicalProcessorCount())
                .withMaxMemory(hardware.getMemory().getTotal())
                .withListDisk(listDisk)
                .withNetworkList(networks)
                .withDistribution(String.valueOf(os));
    }

    @Override
    public InfoEntity getInfo() {
        return infoEntity;
    }
}
