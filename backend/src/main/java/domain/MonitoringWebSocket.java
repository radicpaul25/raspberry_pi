package domain;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.management.OperatingSystemMXBean;
import io.quarkus.scheduler.Scheduled;
import io.quarkus.scheduler.Scheduler;
import org.jboss.logging.Logger;
import oshi.SystemInfo;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@ServerEndpoint("/monitoring")
public class MonitoringWebSocket {

    private static final Logger logger = Logger.getLogger(MonitoringWebSocket.class);
    private Set<Session> socketSessions = new HashSet<Session>();
    Scheduler scheduler;
    public MonitoringWebSocket(Scheduler scheduler)
    {
    }
    public void getCpu()
    {

    }
    @OnOpen
    public void onOpen(Session session) {

        socketSessions.add(session);
        logger.info("Session opened" + session.getId());
        broadcast("ok");
        scheduler.resume("sendMessage");
    }

    @OnClose
    public void onClose(Session session) {
        socketSessions.remove(session);
        if (socketSessions.isEmpty())
        {
            scheduler.pause("sendMessage");
        }
        logger.info("Session closed" + session.getId());
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        socketSessions.remove(session);
        if (socketSessions.isEmpty())
        {
            scheduler.pause("sendMessage");
        }
        logger.info("Error on session" + session.getId());
    }

    @Scheduled(every="1s", identity = "sendMessage")
    public void sendMessage() {

        this.broadcast("ok");
    }
    private void broadcast(String message) {
        socketSessions.forEach(s -> {
            s.getAsyncRemote().sendObject(message, result ->  {
                if (result.getException() != null) {
                    logger.error("Unable to send message: " + result.getException());
                }
            });
        });
    }
}
