package converter.dtoentity;

import converter.Converter;
import domain.entity.InfoEntity;
import presentation.dto.InfoResponse;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class InfoEntityToInfoResponse implements Converter<InfoEntity, InfoResponse> {
    @Override
    public InfoResponse convertNotNull(InfoEntity from) {
        return new InfoResponse()
                .withDistribution(from.getDistribution())
                .withMaxMemory(from.getMaxMemory())
                .withListDisk(from.getListDisk())
                .withNumberOfCores(from.getNumberOfCores())
                .withNetworkList(from.getNetworkList());
    }
}
