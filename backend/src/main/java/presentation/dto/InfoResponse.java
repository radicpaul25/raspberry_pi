package presentation.dto;

import lombok.*;
import util.dataclass.Disk;
import util.dataclass.Network;

import java.util.List;

@With
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class InfoResponse {
    private int numberOfCores;
    private long maxMemory;
    private List<Disk> listDisk;
    private List<Network> networkList;
    private String distribution;
}
