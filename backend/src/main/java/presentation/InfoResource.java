package presentation;

import converter.Converter;
import domain.entity.InfoEntity;
import domain.service.InfoServiceInterface;
import presentation.dto.InfoResponse;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("info")
public class InfoResource {
    @Inject
    InfoServiceInterface infoService;
    @Inject
    Converter<InfoEntity, InfoResponse> infoEntityInfoResponseConverter;
    @GET
    public InfoResponse getInfo()
    {
        return infoEntityInfoResponseConverter.convert(infoService.getInfo());
    }
}
