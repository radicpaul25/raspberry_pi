package util.dataclass;

import lombok.*;

@With
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Network {
    private String name;
    private String ip;
}
