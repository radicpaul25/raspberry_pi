package util.dataclass;

import lombok.*;

@With
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Disk {
    private String name;
    private long size;
}
